import base64
import json
from hamcrest.core.assert_that import assert_that
from hamcrest.core.core.isequal import equal_to
import requests


class IntegrationTests():

    def testImgPost(self):
        headers = {}
        headers["X_REQUESTED_WITH"] = 'XMLHttpRequest'
        headers["CONTENT-TYPE"] = 'application/x-www-form-urlencoded'
        headers["ACCESSTOKEN"] = 'username%3Deceppda%26password%3Dydab2d'
        headers["X-CSRFToken"] = 'wx7YkMddalvFbOeWLzohJEHF3PWc78BC'

        cookies = dict(csrftoken='wx7YkMddalvFbOeWLzohJEHF3PWc78BC')

        init_d = open('/home/eceppda/Pictures/ray.jpg', 'rb').read()

        enc_d = base64.b64encode(init_d)

        requestJson = '{"imgType":"jpg", "imgData" : "'+enc_d+'" }'

        response = requests.post('http://localhost:8000/ajax/post', data={"post" : requestJson}, headers=headers, cookies=cookies)
        print response.text
        assert_that(response.status_code, equal_to(200))

    def testTextPost(self):

        headers = {}
        headers["X_REQUESTED_WITH"] = 'XMLHttpRequest'
        headers["CONTENT-TYPE"] = 'application/x-www-form-urlencoded'
        headers["ACCESSTOKEN"] = 'username%3Deceppda%26password%3Dydab2d'
        headers["X-CSRFToken"] = 'wx7YkMddalvFbOeWLzohJEHF3PWc78BC'

        cookies = dict(csrftoken='wx7YkMddalvFbOeWLzohJEHF3PWc78BC')

        jsonD = '{"text":"make a sense"}'

        response = requests.post('http://localhost:8000/ajax/post', data={"post" : jsonD }, headers=headers, cookies=cookies)
        print response.text
        assert_that(response.status_code, equal_to(200))


it = IntegrationTests()

#it.testTextPost()
it.testImgPost()