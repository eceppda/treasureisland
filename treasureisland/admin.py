from django.contrib import admin
from treasureisland.models import Post

class PostAdmin(admin.ModelAdmin):

    list_display = ('postDt', 'user')

admin.site.register(Post, PostAdmin)