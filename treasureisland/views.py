import Image
import base64
from datetime import datetime
import json
import logging
import urllib
from django.contrib import auth
from django.contrib.auth.models import User
from django.db.utils import DatabaseError
from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response
from kropek import settings
from treasureisland.models import Post
from django.middleware import csrf


logger = logging.getLogger(__name__)

# ajax login decorator
def ajax_login_required(f):
    def new_f(request):

        logging.debug('authenticating request')

        token = request.META.get('HTTP_ACCESSTOKEN', '')

        decoded_token = urllib.unquote(token)

        logging.debug("received token: %s" % decoded_token )

        params = dict([part.split('=') for part in decoded_token.split('&')])
        username = params['username']
        passwd = params['password']

        try:
            logging.debug('checking username')
            # try to obtain the username from the database
            user = User.objects.get(username__exact=username)

        except User.DoesNotExist:
            # if there is no user, clear the variable
            username=''

        logging.debug('authenticating user')
        user = auth.authenticate(username=username, password=passwd)

        if user is not None:

            auth.login(request, user)

        else:
            return render_to_response('treasureisland/fail.json', {'message':'know your password'})

        return f(request)
    return new_f

def base64_url_decode(inp):
    inp = inp.replace('-','+').replace('_','/')
    padding_factor = (4 - len(inp) % 4) % 4
    inp += "="*padding_factor
    return base64.decodestring(inp)


# ajax-based views
@ajax_login_required
def post(request):

    if request.is_ajax():

        if request.user.is_authenticated():

            if request.method == 'POST':
                # TODO - log this instead
                #print 'Raw Data: "%s"' % request.raw_post_data
                # this doesn't work -- st = request.POST.get('post').replace('/', '//')

                data =json.loads(request.POST.get('post'))


                if data is not None:

                    # TODO - validate the json here!

                    try:
                        p = Post()
                        p.user = request.user
                        if data.get('caption'):
                            p.caption = data.get('caption')
                        if data.get('imgData') and data.get('imgType'):
                            data = doTheImage(data)
                            p.imgData = data.get('imgData')
                            p.imgType = data.get('imgType')
                            p.imgThumbnail = data.get('imgThumbnail')
                        if data.get('vidUrl'):
                            p.vidUrl = data.get('vidUrl')
                        if data.get('text'):
                            p.text = data.get('text')
                        if data.get('mp3Url'):
                            p.mp3Url = data.get('mp3Url')

                        p.save()

                    except DatabaseError:
                        render_to_response('treasureisland/fail.json', {'message':'unable to add post'})

                return render_to_response('treasureisland/success.json')
            else:
                return render_to_response('treasureisland/fail.json', {'message':'unsupported protocol'}, mimetype="application/json")
        else:
            return render_to_response('treasureisland/fail.json', {'message':'User is not authenticated'})
    else:
        return render_to_response('treasureisland/fail.json', {'message':'Ajax request is required'})

def doTheImage(data):
#    First, write the full image to a tmp file
    fileExt = data.get('imgType')
    fileTStamp = datetime.strftime(datetime.today(), "%Y%m%d%H%M%S")
    fileToWrite = fileTStamp + '.' + fileExt
    file_path = getattr(settings,'FILE_UPLOAD_TEMP_DIR')+'/'+fileToWrite

    imgData = data.get('imgData')
    fh = open(file_path, "wb")
    fh.write(imgData.decode('base64'))
    fh.close()

#    Next convert to thumbnail size
    im1 = Image.open(file_path)
    width = 300
    height = 320
    im5 = im1.resize((width, height), Image.ANTIALIAS) # best down-sizing filter
    tNailToWrite = getattr(settings,'FILE_UPLOAD_TEMP_DIR')+'/'+fileTStamp+"tnail."+fileExt
    im5.save(tNailToWrite)

#    Encode the tnail
    fileThumb = open(tNailToWrite, 'rb').read()

    encodedThumb = base64.b64encode(fileThumb)

#    Save thumbnail to data
    data['imgThumbnail'] = encodedThumb

    return data


def get_csrf(request):
    if request.method == 'GET':
        csrf_token = csrf.get_token(request)
        return HttpResponse("{ 'token': '" + csrf_token + "'}")
    else:
        return render_to_response('treasureisland/fail.json', {'message':'go away punk'})


# web views
def index(request):
    try:
        posts = Post.objects.all().order_by('-postDt')[:5]
    except Post.DoesNotExist:
        posts = None
    return render_to_response('index.html', {'posts': posts})

def detail(request, post_id):
    try:
        p = Post.objects.get(pk=post_id)
    except Post.DoesNotExist:
        raise Http404
    return render_to_response('detail.html', {'post': p})