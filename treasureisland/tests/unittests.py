import logging
from django.test import TestCase
from django.test.client import Client


class PostTest(TestCase):

    logger = logging.getLogger(__name__)

    fixtures = ['initial_data.yaml']

    kwargs = {}
    kwargs["HTTP_X_REQUESTED_WITH"] = 'XMLHttpRequest'
    kwargs["CONTENT-TYPE"] = 'application/x-www-form-urlencoded'
    kwargs["HTTP_ACCESSTOKEN"] = 'username%3Deceppda%26password%3Dydab2d'
    kwargs["X-CSRFToken"] = '8p1XqVFAYnZgsR3jXeX1kXGbMMYNYvY1'


    def setUp(self):
        self.client = Client()

    def test_add_text(self):
        """
        Tests that the api supports posting text
        """
        requestJson = '{"text": "something special"}'
        resp = self.client.post('/ajax/post', {'post': requestJson}, **self.kwargs)
        self.assertEqual(resp.status_code, 200)

    def test_check_csrf(self):
        """
        Tests that the api supports getting csrf
        """
        resp = self.client.get('/ajax/csrf')
        self.assertEqual(resp.status_code, 200)
