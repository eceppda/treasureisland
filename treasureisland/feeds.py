from django.contrib.syndication.views import Feed
from treasureisland.models import Post

class TreasureFeed(Feed):
    '''Feed for latest 15 blog entries'''
    title='Treasure Island'
    link='http://treasure.kropek.org'
    description='IDGASSS'

    item_author_name='TREASURER'
    item_author_email='treasures@reasable.com'
    item_author_link='http://treasure.kropek.org'

    def items(self):
        return Post.objects.all().order_by('-postDt')[:15]

    def item_pubdate(self,item):
        return item.postDt