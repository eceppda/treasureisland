from django.contrib.auth.models import User
from django.db import models

class Post(models.Model):

    postDt = models.DateTimeField(auto_now_add=True)
    caption = models.CharField(max_length=300, blank=True)
    user = models.ForeignKey(User)
    imgData = models.TextField(blank=True)
    imgThumbnail = models.TextField(blank=True)
    imgType = models.CharField(max_length=5, blank=True)
    vidUrl = models.URLField(blank=True)
    text = models.TextField(blank=True)
    mp3Url = models.URLField(blank=True)

    def __unicode__(self):
        return unicode(self.postDt)

    def get_absolute_url(self):
        return "/post/%i/" % self.id
