from django.conf.urls import patterns, include, url

from django.contrib import admin
from treasureisland.feeds import TreasureFeed

admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'treasureisland.views.index', name='index'),
    url(r'^feed$', TreasureFeed()),
    url(r'^post/(?P<post_id>\d+)/$', 'treasureisland.views.detail'),

#    url(r'^ajax/latest', 'treasureisland.views.latest'),
    url(r'^ajax/post$', 'treasureisland.views.post'),
    url(r'^ajax/csrf$', 'treasureisland.views.get_csrf'),


    url(r'^admin/', include(admin.site.urls)),
)
